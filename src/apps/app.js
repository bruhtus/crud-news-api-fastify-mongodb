const Fastify = require('fastify');

const db = require('../plugins/db');
const config = require('../config');
const route = require('../plugins/routes');

function app() {
  const fastify = Fastify();

  fastify.register(db, { uri: config.mongoUri() });
  fastify.register(route, { prefix: 'api/news' })

  return fastify;
}

module.exports = app;
