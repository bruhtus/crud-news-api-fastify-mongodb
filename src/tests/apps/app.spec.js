const mongoose = require('mongoose');

const build = require('../../apps/app');

const app = build();

beforeAll(async () => {
  // wait until all the plugin have been loaded.
  await app.ready();
});

afterAll(async () => {
  // ref:
  // https://mongoosejs.com/docs/api/connection.html#connection_Connection-readyState
  if (mongoose.connection.readyState === 1) {
    const collections = mongoose.connection.collections;

    // clear documents in database.
    // only clear database if mongoose connected.
    for (key in collections) {
      const collection = collections[key];
      await collection.deleteMany();
    }

    // close connection to database.
    // only close database if mongoose connected.
    await mongoose.connection.dropDatabase()
    await mongoose.connection.close()
  }
});

async function getNewsId() {
  const newsId = await app.inject({
    method: 'GET',
    url: 'api/news'
  });

  return newsId.json()[0]._id
}

describe('CRUD news exist', () => {
  const news = {
    title: 'jakarta banjir',
    content: 'jakarta mulai tenggelam',
    topics: [ 'bencana manusia', 'tradisi' ],
    status: 'publish'
  };

  const newNewsContent = 'jakarta tenggelam';

  test('can create news', async () => {
    const res = await app.inject({
      method: 'POST',
      url: 'api/news',
      payload: news
    });

    expect(res.statusCode).toBe(201);
    expect(res.json()).toMatchObject(news);
  });

  test('can find news by id', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'GET',
      url: `api/news/${id}`
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toMatchObject(news);
  });

  test('can find news by status', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
      query: { status: news.status }
    });

    // the output of find() is an array.
    // ref:
    // https://medium.com/@andrei.pfeiffer/jest-matching-objects-in-array-50fe2f4d6b98
    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual(
      expect.arrayContaining([
        expect.objectContaining(news)
      ])
    );
  });

  test('can find news by topic', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
      query: { topics: news.topics[0] }
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual(
      expect.arrayContaining([
        expect.objectContaining(news)
      ])
    );
  });

  test('can update news based on content', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'PUT',
      url: `api/news/${id}`,
      payload: { content: newNewsContent }
    });

    expect(res.statusCode).toBe(201);
    expect(res.json()).toMatchObject({
      ...news,
      content: newNewsContent
    });
  });

  test('can denied post request without title', async () => {
    const { title, ...newsWithoutTitle } = news;
    const res = await app.inject({
      method: 'POST',
      url: 'api/news',
      payload: newsWithoutTitle
    });

    expect(res.statusCode).toBe(400);
  });

  test('can denied put request without request body', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'PUT',
      url: `api/news/${id}`,
    });

    expect(res.statusCode).toBe(400);
  });

  test('can delete news based on id', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'DELETE',
      url: `api/news/${id}`,
    });

    expect(res.statusCode).toBe(200);
  });

  test('cannot find deleted news', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
    });

    const findNews = res.json().filter(n => n.title === news.title)

    expect(res.statusCode).toBe(200);
    expect(findNews.length).toBe(0);
  });
})
