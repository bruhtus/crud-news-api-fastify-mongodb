const fp = require('fastify-plugin');
const mongoose = require('mongoose');
const config = require('../config');

// connect to database
async function db(fastify, config, next) {
  const connect = await mongoose.connect(config.uri, {
    serverSelectionTimeoutMS: 1000
  });

  fastify.decorate('dbConnect', connect);

  next();
}

module.exports = fp(db);
