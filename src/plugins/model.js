const mongoose = require('mongoose');

const newsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  content: String,
  topics: [String],
  status: String,
});

const model = mongoose.model('news', newsSchema);

module.exports = model;
