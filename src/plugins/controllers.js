function crudControllers(model) {
  async function createOne(req, res) {
    const doc = await model.create(req.body);
    return res.status(201).send(req.body);
  }

  async function getAll(req, res) {
    const docs = await model.find(req.query);
    return res.status(200).send(docs);
  }

  async function getOne(req, res) {
    const doc = await model.findById({
      _id: req.params.id
    });
    return res.status(200).send(doc);
  }

  async function updateOne(req, res) {
    const updated = await model.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );

    return res.status(201).send(updated);
  }

  async function removeOne(req, res) {
    const deleted = await model.findByIdAndRemove({
      _id: req.params.id
    });
    return res.status(200).send(deleted);
  }

  return { createOne, getAll, getOne, updateOne, removeOne };
}

module.exports = crudControllers;
