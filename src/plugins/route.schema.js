const postRequest = {
  type: 'object',
  required: ['title'],
  properties: {
    title: {
      type: 'string'
    },
    content: {
      type: 'string'
    },
    topics: {
      type: 'array'
    },
    status: {
      type: 'string'
    },
  }
};

const putRequest = {
  type: 'object',
  required: ['content'],
  properties: {
    content: {
      type: 'string'
    },
  }
};

module.exports = { postRequest, putRequest };
