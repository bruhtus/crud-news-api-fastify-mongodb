const fp = require('fastify-plugin');

const model = require('../plugins/model');
const controllers = require('./controllers');
const { postRequest, putRequest } = require('./route.schema');

const {
  createOne,
  getAll,
  getOne,
  updateOne,
  removeOne
} = controllers(model);

function route(fastify, config, next) {
  const getEverything = fastify.get('/', getAll);

  const getItem = fastify.get('/:id', getOne);

  const postOne = fastify.post(
    '/',
    { schema: { body: postRequest } },
    createOne
  );

  const putOne = fastify.put(
    '/:id',
    { schema: { body: putRequest }},
    updateOne
  );

  const deleteOne = fastify.delete('/:id', removeOne);

  next();
}

module.exports = route;
