function mongoUri() {
  return process.env.MONGO_URI;
}

module.exports = { mongoUri };
