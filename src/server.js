const build = require('./apps/app');

async function server() {
  // make sure all the plugin is ready.
  const app = await build().ready();
  const PORT = process.env.PORT || 8080;

  app.listen(PORT);
}

server();
