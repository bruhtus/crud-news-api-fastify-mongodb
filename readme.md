## References

- [Mongoose server selection timeout](https://mongoosejs.com/docs/connections.html#server-selection).
- [Jest matching object in array medium post](https://medium.com/@andrei.pfeiffer/jest-matching-objects-in-array-50fe2f4d6b98).
- [Example how to shutting down the connection with fastify](https://github.com/fastify/fastify/issues/1367#issuecomment-453206962).
- [Fastify request documentation](https://github.com/fastify/fastify/blob/main/docs/Reference/Request.md).
- [Setup github CI blog post](https://dev.to/6thcode/how-to-set-up-a-cicd-environment-on-gitlab-using-nodejs-jh3).
- [Setup env for gitlab CI](https://stackoverflow.com/questions/42966645/setting-up-postgresql-in-gitlab-ci).
- [Setup mongodb cluster with heroku](https://www.mongodb.com/developer/how-to/use-atlas-on-heroku/).
